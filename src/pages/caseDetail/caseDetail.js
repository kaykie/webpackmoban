/**
 * Created by Administrator on 2017/7/3.
 */
import headerTpl from '../../assets/components/header/header';
import footerTpl from '../../assets/components/footer/footer';

$(function () {
    var footer =new footerTpl();
    $('.footer').html(footer.footer);
    var header = new headerTpl();
    $('.header').html(header.header);

    function GetQueryString(name)
    {
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if(r!=null)return  unescape(r[2]); return null;
    }

     var id = GetQueryString("id");
    $.ajax({
        url: "http://localhost:9080/app2/app2/web/product/detail",
        type: 'get',
        data: {'id': id},
        success: function (data) {
            var product = data.results.product;
            var left = "";
            var right = "";
            for(var i = 0; i < product.htmls.length; i++){
                if(product.htmls[i].position == "left"){
                    left = product.htmls[i].html;
                }
                if(product.htmls[i].position == "right"){
                    right = product.htmls[i].html;
                }
            }
            console.log(left);
            console.log(right);
            $("#insetSectionLeft").html(left);
            $("#insetSectionRight").html(right);

            var slide = [];
            var slideDown = [];
            for(var i = 0; i < product.images.length; i++){
                if(product.images[i].position == "slide"){
                    slide.push(product.images[i].address);
                }
                if(product.images[i].position == "slideDown"){
                    slideDown.push(product.images[i].address);
                }
            }
            for(var i = 0; i < slide.length; i++){
                if(i < slide.length - 1){
                    $("#slide").append(" <div class='swiper-slide red-slide swiper-slide-2'><img src='http://localhost:9080/app2/"+slide[i]+"' alt=''> </div>");
                }else{
                    $("#slide").append(" <div class='swiper-slide red-slide swiper-slide-3'><img src='http://localhost:9080/app2/"+slide[i]+"' alt=''> </div>");
                }
            }
            for(var i = 0; i < slideDown.length; i++){
                $("#slideDown").append(" <div class='col-6'><img src='http://localhost:9080/app2/"+slideDown[i]+"' alt=''></div>");
            }
        }
    })
});