/**
 * Created by Administrator on 2017/6/28.
 */
/**
 * Created by Administrator on 2017/5/29.
 */
var webpack = require("webpack");
var htmlWebpackPlugin = require("html-webpack-plugin");
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var OpenBrowserPlugin = require('open-browser-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
//nodejs的里的一个全局变量,它指向的是我们项目的根目录,入口文件的位置
module.exports = {
    // devtool: "eval-source-map",
    //配置入口文件
    entry: {
        main: __dirname + '/app/main.js',

        // chunk:[__dirname + '/app/js/chunk1.js',__dirname + '/app/js/chunk2.js'],
        _jquery: ['jquery',__dirname+'/app/js/chunk3.js']
    },

    output: {
        //需要打包后的文件 放置的位置
        path: __dirname + '/public',
        //打包后文件的名字
        // filename: 'js/[name].[chunkHash:5].js',
        filename: 'js/[name].js',
        // publicPath: "http://www.xianianwang.com" //用来上线时候输出的前缀
    },


    module: {
        loaders: [
            {
                test: /\.json$/,
                use: 'json-loader'
            },
            //处理页面中以ejs为结尾的文件
            {
                test: /\.ejs$/,
                use: 'ejs-loader'
            },
            //处理页面中以html为结尾的文件,使用了这个插件,ejs中的变量将不再有效果
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            //下面这个插件会将页面中的变量解析为正常的html文件,如何要在页面中使用htmlwebpackplugin变量则需要注释这个插件
            // {
            //   test: /\.css$/,
            //   //从右向左翻译,css-loader必须在右边,不然没办法执行style-loader
            //   use:[
            //     'style-loader', 'css-loader',
            //     {
            //     loader: "postcss-loader",
            //     options:{
            //       plugins: (loader) => [
            //         require('autoprefixer')
            //         ],
            //         minimize:true
            //     }
            //   }]
            // },
            //处理图片
            {
                test: /\.(png|jpg|git|woff)/,
                use: [
                    //将图片输出到images的目录之下如何大小在8192之下则打包成base64,如果在这这之前刚保存到./images 之下,
                    // 注意：publicPath是指在页面上显示的地址，当其改为../的时候，所有页面调用图片的时候，前面的路径都会变成../
                    {
                        loader: "url-loader?limit=8192?name=[name].[hash:6].[ext]&publicPath=./&outputPath=images/"
                    },
                    //压缩图片的大小
                    {
                        loader: "image-webpack-loader"
                    }
                ]
            },
            //将es6的代码转为es5的代码
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            //处理css:extractTextPlugin负责把css独立出来,style-loader，以什么形式处理css,css-loader压缩css样式，post-loader给其添加前缀
            {
                test: /\.css$/, loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        minimize: true
                    }
                },
                    {
                        loader: "postcss-loader", options: {
                        plugins: (loader) => [
                            require('autoprefixer')
                        ],
                        minimize: true
                    }
                    }]
            })
            }
        ],
    },
    plugins: [
        new ExtractTextPlugin({filename: 'css/[name].css'}),//样式压缩
        //创建一个新的页面在public 以app/index.html中的文件为依据
        //   生产环境下启用：压缩js代码
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings: false
          },
          mangle: false,
        }),
          new webpack.ProvidePlugin({
              $: "jquery",
              jQuery: "jquery",
              "window.jQuery": "jquery"
          }),
        new htmlWebpackPlugin({
            filename: 'index.html',
            template: "app/index.html",
            title: "webpack title",
            inject: 'body',
            date: new Date(),
            minify: {
                removeComments: true,    //移除HTML中的注释
                // collapseWhitespace: true,    //删除空白符与换行符
                conservativeCollapse: true,
                minifyJS: true //js也在一行
            },
            // hash: true,
            // chunks: ['index'],
            // xhtml: true,
            // showErrors: true
        }),
        new htmlWebpackPlugin({
            filename: 'inset/aboutus.html',
            template: "app/inset/aboutus.html",
            title: "铁友轨道交通-联系我们页面",
            inject: 'body',
            minify: {
                removeComments: true,    //移除HTML中的注释
                // collapseWhitespace: true,    //删除空白符与换行符
                // conservativeCollapse: true,
                minifyJS: true //js也在一行
            },

            // hash: true,
            // chunks: ['index'],
            // xhtml: true,
            // showErrors: true
        }),
        //单独打包css后 是无法使用热更新css样式的
        //   new webpack.HotModuleReplacementPlugin(),
        new CommonsChunkPlugin({
            //将公共模块打包到_jquery.js中，这里的文件以来于入口的_jquery，如果其发生改变 那么_jquery的hash值也会发生改变
            //chunk.js是使文件在浏览器中得以缓存，方便其它页面的应用加载，省了大量的流量。
            name: ['_jquery','chunk'],
            // filename:'./js/chunk.js'
            // minChunks: 2
        }),
        //自动打开浏览器
        new OpenBrowserPlugin({
            url: 'http://localhost:8079'
        }),
        //添加版权信息
        new webpack.BannerPlugin("Copyright by xianianwang 2038212003@qq.com")
    ],
    devServer: {
        contentBase: "./public",//本地服务器所加载的页面所在的目录
        stats: {
            colors: true,//终端中输出结果为彩色---没有颜色了
        },
        historyApiFallback: true,//不跳转
        inline: true,//实时刷新
        port: 8079,
        // hot:true
    },
};
// var htmlWebpackPlugin=new htmlWebpackPlugin();